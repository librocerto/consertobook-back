import dataclasses
import datetime
import typing
from enum import Enum

from redis_om import HashModel, JsonModel, EmbeddedJsonModel


class User(HashModel):
    name: str
    lastname: str
    email: str
    password: str


class Category(Enum):
    ADVENTURE = 1
    STORIES = 2
    CLASSICS = 3
    CRIME = 4
    TALES = 5
    FANTASY = 6
    HISTORICAL = 7
    FICTION = 8
    HORROR = 9
    HUMOUR = 10
    SATIRE = 11
    MYSTERY = 12
    POETRY = 13
    PLAYS = 14
    ROMANCE = 15
    SHORT_STORIES = 16
    THRILLERS = 17
    WAR = 18
    SCIENCE_FICTION = 19
    YOUNG = 20
    AUTOBIOGRAPHY = 21
    AND = 22
    MEMOIR = 23
    BIOGRAPHY = 24
    ESSAYS = 25
    NOVEL = 26
    COMPUTING = 27
    FAIRY = 28
    LITERARY_FICTION = 29


@dataclasses.dataclass
class BorrowersHistory:
    user: str  # user PK
    borrow_date: datetime.datetime
    end_date: typing.Optional[datetime.datetime] = None
    degradations: typing.Optional[str] = None


class Book(JsonModel):
    title: str
    description: str
    tags: list[Category]  # list of str
    owner: str  # User PK
    borrowers: list[BorrowersHistory]
    is_borrowed: bool
    language: str
