# consertobook-back


## Getting started

### Setup env
`poetry install` ('--without-dev') # install l'ensemble des dépendence présentes dans le pyproject.toml  

### To run the project
`docker compose up -d` run redis db
`strawberry server schema` lancer le serveur d'application

### Utils
`poetry run` lance la commande donnée dans l'environnement créé pour le projet  
`poetry shell` lance une nouvelle shell avec l'environnement virtuel créé pour le projet  
`deactivate` désactive l'environnement virtuel  
`ipython` lance un interpreteur python améliorer  
`http localhost:8000/graphql query="query{books{pk owner{pk name}}}"` query en http type curl  
`http --help` 

### Utils packages
#### httpie
https://httpie.io/  
prettyfyed curl client  
#### pipx
https://pypa.github.io/pipx/  
python cli tools manager  


## TODO for next sessions


## TODO for later
[ ] foreign key validation
[ ] authentication by jwt

## DEPEDENCIES
Wait to upgrade pydantic to version 2.3.0 because of breaking change, stay on 1.10.12 version until it's not fix