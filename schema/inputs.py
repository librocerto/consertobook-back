import typing

import strawberry

from schema.types import CategoryType, BorrowersHistoryType


@strawberry.input
class UserMutateInput:
    pk: typing.Optional[str] = None
    name: typing.Optional[str] = None
    lastname: typing.Optional[str] = None
    email: typing.Optional[str] = None
    password: typing.Optional[str] = None


@strawberry.input
class BookMutateInput:
    pk: typing.Optional[str] = None
    title: typing.Optional[str] = None
    description: typing.Optional[str] = None
    tags: typing.Optional[list[str]] = None  # list of str
    owner: typing.Optional[str] = None  # User PK
    is_borrowed: typing.Optional[bool] = None
    language: typing.Optional[str] = None
