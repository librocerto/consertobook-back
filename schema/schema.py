import dataclasses
import datetime
import http
import typing

import strawberry

import models
from schema.inputs import UserMutateInput, BookMutateInput
from schema.types import BookType, UserType


# TODO user authentication


@strawberry.type
class Query:
    @strawberry.field
    def books(self) -> list[BookType]:
        """
        TODO is_borrowed: bool = None filter
        :return:
        """

        book_list = models.Book.find().all()

        return [BookType.from_pydantic(i) for i in book_list]

    @strawberry.field
    def book(self, book_pk: str) -> BookType:
        return BookType.from_pydantic(models.Book.get(book_pk))

    @strawberry.field
    def users(self) -> list[UserType]:
        user_list = models.User.find().all()

        return [UserType.from_pydantic(i) for i in user_list]

    @strawberry.field
    def user(self, user_pk: str) -> UserType:
        return UserType.from_pydantic(models.User.get(user_pk))


@strawberry.type
class Mutation:
    @strawberry.field
    def user(self, mutated_user: UserMutateInput) -> UserType:
        """
        This function allows you to add or update a user
            if user exist in bdd : update it
            if not: create it

        used functions:
            dataclasses.asdict: take a dataclass in argument and return a dict with it's fields
            {}.update({}: dict)
            models.User.get(pk)
            user = models.User(** user_create_input.__dict__)
            {}.items -> list[tuple[key, value]]

            [i for i in n]: génère une liste à la volée basée sur l'itérable n

            for i in range(10):
                print(i)

        :param mutated_user:
        :return: UserType
        """
        if mutated_user.pk:
            # If pk, user exist so just update it
            user = models.User.get(mutated_user.pk)
            user.__dict__.update(
                {
                    key: value
                    for key, value in dataclasses.asdict(mutated_user).items()
                    if value is not None
                }
            )
        else:
            # if no pk, so create a new User
            user = models.User(**dataclasses.asdict(mutated_user))

        user.save()

        return UserType.from_pydantic(user)

    @strawberry.field
    def delete_user(self, pk: str) -> str:
        models.User.delete(pk)

        return pk

    @strawberry.field
    def book(self, mutated_book: BookMutateInput) -> BookType:
        if mutated_book.pk:
            book = models.Book.get(mutated_book.pk)
            mutated_book.__dict__.update(
                {
                    key: value  # create a new index with key: value at each loop iteration
                    # boucle sur l'ensemble des key value du dictionnaire
                    for key, value in dataclasses.asdict(mutated_book).items()
                    if value is not None
                }
            )
        else:
            book = models.Book(**dataclasses.asdict(mutated_book))

        book.save()

        return BookType.from_pydantic(book)

    @strawberry.field
    def delete_book(self, pk: str) -> str:
        models.Book.delete(pk)

        return pk

    @strawberry.field
    def borrow_book(self, book_pk: str, user_pk: str) -> BookType:
        borrower_history = models.BorrowersHistory(
            user=user_pk,
            borrow_date=datetime.datetime.now(),
        )
        book = models.Book.get(book_pk)
        book.borrowers.append(borrower_history)
        book.is_borrowed = True
        book.save()

        return BookType.from_pydantic(book)

    @strawberry.field
    def book_return(self, book_pk: str, degradations: typing.Optional[str]) -> BookType:
        book = models.Book.get(book_pk)
        borrower_history = book.borrowers[-1]
        borrower_history.end_date = datetime.datetime.now()
        borrower_history.degradations = degradations
        book.is_borrowed = False
        book.save()

        return BookType.from_pydantic(book)


schema = strawberry.Schema(query=Query, mutation=Mutation)
