import dataclasses
import typing

import strawberry

import models


@strawberry.experimental.pydantic.type(model=models.User, all_fields=True)
class UserType:
    """
    i inherit my fields from models.User thanks to @strawberry.experimental.pydantic.type
    the argument all_fields=True say i copy all field drom my mother class and overwrite all that are
    """

    # TODO handle read right depending of the user
    pass


@strawberry.type
class BorrowersHistoryType(models.BorrowersHistory):
    """
    TODO map user property
    """

    pass


# create a CategoryType strawberry enum from models.Category
CategoryType = strawberry.enum(models.Category)


@strawberry.experimental.pydantic.type(model=models.Book)
class BookType:
    pk: str
    title: strawberry.auto
    description: strawberry.auto
    tags: list[CategoryType]
    owner: UserType
    borrowers: list[BorrowersHistoryType]
    is_borrowed: strawberry.auto
    language: strawberry.auto

    @staticmethod
    def from_pydantic(
        instance: models.Book, extra: dict[str, typing.Any] = None
    ) -> "BookType":
        """
        take a Redis_om Model and return a StrawberyType

        parse the owner primary key to return the related User object and parse it to a UserType object using it's
        .from_pydantic() method

        instance.dict() return the dictionnary representaiton  of the instance
        ex:
            {
                "pk": "pketdfgsdg",
                "title": "truc",
                "description": "truc",
                "tags": [],
                "owner": "pk_ownerfdsfdgbfdsg",
                is_borrowed: False,
                language: "FR",
            }

        **data unpack the data dictionnary and map each key on the related argument
        ex: the key {"pk": "pk_fdgdsgfsg"} is mapped to the argument "pk" with value "pk_fdgdsgfsg"

        :param instance: the Redis_om Model to parse
        :param extra:
        :return: a new instance of StrawberryType created with the data of the instance.
        """
        data = instance.dict()
        data["owner"] = UserType.from_pydantic(models.User.get(data["owner"]))
        new_book = BookType(**data)
        return new_book

    def to_pydantic(self) -> models.Book:
        """
        self: the instance you're working on, handle the data and methods of the current object
        This method take a BookType instance and return a Book model, in the model, the owner is a str type (pk)
        so we need to convert the UserType.owner to a str pk in the Book's owner field

        :return: an instance of book model
        """
        # return all fields of the instance without the methods and transform the instance into a dict
        data = dataclasses.asdict(self)

        owner = self.owner.pk
        models_book = models.Book(**data)
        models_book.owner = owner

        return models_book
